section .text


; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
    cmp byte [rdi + rax], 0
    je .end
    inc rax
    jmp .loop
.end:
    ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    call string_length
    mov rdx, rax
    mov rsi, rdi
    mov rdi, 1
    mov rax, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax
    push rdi
    mov rsi, rsp
    mov rax, 1
    mov rdi, 1
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
    xor r8, r8
    mov rax, rdi
    mov r9, 0xA
.loop:
    xor rdx, rdx
    div r9
    inc r8
    push rdx
    cmp rax, 0
    je .print_digit
    jmp .loop
.print_digit:
    pop rdx
    add rdx, 0x30
    mov rdi, rdx
    push rcx
    call print_char
    pop rcx
    dec r8
    jnz .print_digit
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    xor rax, rax
    test rdi, rdi
    jge .print_digits
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
.print_digits:
    call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor rcx, rcx
.loop:
    mov r8b, byte [rdi + rax]
    cmp byte [rsi + rax], r8b
    jnz .not_equals
    cmp byte [rdi + rax], 0
    jz .equals
    cmp byte [rsi + rax], 0
    inc rax
    jnz .loop
.equals:
    mov rax, 1
    ret
.not_equals:
    xor rax, rax
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    push 0
    mov rsi, rsp
    xor rdi, rdi
    mov rdx, 1
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rax, rax
    xor r9, r9
    xor rcx, rcx
.loop:
    push rdi
    push rsi
    push rcx
    call read_char
    pop rcx
    pop rsi
    pop rdi
    cmp rax, 0x0
    jz .valid
    cmp rax, 0x20
    jz .check_space
    cmp rax, 0x9
    jz .check_space
    cmp rax, 0xA
    jz .check_space
    inc r9
    cmp r9, rsi
    jz .overflow
    mov rcx, 1
    mov [rdi + r9 - 1], rax
    jmp .loop
.check_space:
    test rcx, rcx
    jz .loop
    jnz .valid
.valid:
    mov rax, rdi
    mov rdx, r9
    jmp .end
.overflow:
    xor rax, rax
.end:
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor rcx, rcx
    xor r8, r8
    mov r9, 0xA
.loop:
    mov r8b, [rdi + rcx]
    cmp r8b, 0x30
    jl .end
    cmp r8b, 0x39
    jg .end
    sub r8b, 0x30
    inc rcx
    mul r9
    add rax, r8
    jmp .loop
.end:
    mov rdx, rcx
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor r8, r8
    mov r8b, [rdi]
    cmp r8b, 0x2D
    jne .parse_uint_without_sign
    inc rdi

.parse_uint_without_sign:
    push r8
    call parse_uint
    pop r8
    cmp rdx, 0
    je .end
    cmp r8b, 0x2d
    jne .end
.negative:
    neg rax
    inc rdx
.end:
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    call string_length
    cmp rax, rdx
    jge .overflow
    xor r9, r9
.loop:
    mov r8b, byte [rdi + r9]
    mov [rsi + r9], r8b
    inc r9
    cmp r8b, 0
    je .end
    jmp .loop
.overflow:
    xor rax, rax
.end:
    ret
